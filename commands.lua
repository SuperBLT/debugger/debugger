-- We have to jump through a hoop with Serpent - it'll happily reference
--  anything in it's global namespace by name, and this ends up generating
--  errors in the IDE.
--
-- Therefore, we have to copy across just the vanilla Lua stuff
local serpent_global = {}

-- Pulled from PIL Lua 5.3, should work fine
-- A good few of these will be missing, and that just sets them to nil
local lenv = getfenv(1)
for _, name in ipairs({
    "collectgarbage", "type", "io",
    "tostring", "loadstring", "coroutine",
    "_G", "module", "select",
    "ipairs", "error", "setmetatable",
    "dofile", "_VERSION", "arg",
    "package", "next", "getmetatable",
    "assert", "rawget", "require",
    "table", "rawequal", "unpack",
    "string", "math", "rawset",
    "loadfile", "load", "utf8",
    "debug", "os", "bit32",
    "xpcall", "pairs", "rawlen",
    "print", "pcall", "tonumber",
}) do
  serpent_global[name] = lenv[name]
end

serpent_global._G = serpent_global

local serpent = loadfile("serpent.lua")
setfenv(serpent, serpent_global)
serpent = serpent()

local function stringify_results(params, status, ...)
  if not status then return status, ... end -- on error report as it

  params = params or {}
  if params.nocode == nil then params.nocode = true end
  if params.comment == nil then params.comment = 1 end

  local t = {...}
  for i,v in pairs(t) do -- stringify each of the returned values
    local ok, res = pcall(serpent.line, v, params)
    t[i] = ok and res or ("%q"):format(res):gsub("\010","n"):gsub("\026","\\026")
  end
  -- stringify table with all returned values
  -- this is done to allow each returned value to be used (serialized or not)
  -- intependently and to preserve "original" comments
  return pcall(serpent.dump, t, {sparse = false})
end

local function set_breakpoint(file, line)
  if file == '-' and lastfile then
    file = lastfile
  end
  if not breakpoints[line] then
    breakpoints[line] = {}
  end
  breakpoints[line][file] = true
end

local function remove_breakpoint(file, line)
  if file == '-' and lastfile then
    file = lastfile
  elseif file == '*' and line == 0 then
    breakpoints = {}
  end
  if breakpoints[line] then
    breakpoints[line][file] = nil
  end
end

local function stack(start)
  -- Account for ourselves as a function call
  start = start + 1

  local function vars(f)
    local func = debug.getinfo(f, "f").func
    local i = 1
    local locals = {}
    -- get locals
    while true do
      local name, value = debug.getlocal(f, i)
      if not name then break end
      if string.sub(name, 1, 1) ~= '(' then
        locals[name] = {value, select(2,pcall(tostring,value))}
      end
      i = i + 1
    end
    -- get varargs (these use negative indices)
    i = 1
    while true do
      local name, value = debug.getlocal(f, -i)
      -- `not name` should be enough, but LuaJIT 2.0.0 incorrectly reports `(*temporary)` names here
      if not name or name ~= "(*vararg)" then break end
      locals[name:gsub("%)$"," "..i..")")] = {value, select(2,pcall(tostring,value))}
      i = i + 1
    end
    -- get upvalues
    i = 1
    local ups = {}
    while func do -- check for func as it may be nil for tail calls
      local name, value = debug.getupvalue(func, i)
      if not name then break end
      ups[name] = {value, select(2,pcall(tostring,value))}
      i = i + 1
    end
    return locals, ups
  end

  local stack = {}
  local linemap = ildebug.linemap
  for i = (start or 0), 100 do
    local source = debug.getinfo(i, "Snl")
    if not source then break end

    local src = source.source
    if src:find("@") == 1 then
      src = src:sub(2):gsub("\\", "/")
      if src:find("%./") == 1 then src = src:sub(3) end
    end

    table.insert(stack, { -- remove basedir from source
      {source.name, removebasedir(src, basedir),
       linemap and linemap(source.linedefined, source.source) or source.linedefined,
       linemap and linemap(source.currentline, source.source) or source.currentline,
       source.what, source.namewhat, source.short_src},
      vars(i+1)})
    if source.what == 'main' then break end
  end
  return stack
end

--------

local function process_cmd(info)
  --server:settimeout(not blocking and 0 or nil)

  local line, err = server:receive()
  if not line then
    if err == "closed" then
      error("Debugger connection closed", 0)
  --[[
    elseif err == "timeout" then
      assert(not blocking)
      return cmd_results.NO_CMD
  --]]
    else
      error("Unknown receive error " .. tostring(err))
    end
  end

  --server:settimeout(nil)

  command = string.sub(line, string.find(line, "^[A-Z]+"))
  if command == "SETB" then
    -- Note string.find returns the start and end position, then the captures, so this
    --  comes out to start_pos,end_pos,cmd,file,line
    local _, _, _, file, line = string.find(line, "^([A-Z]+)%s+(.-)%s+(%d+)%s*$")
    if file and line then
      set_breakpoint(file, tonumber(line))
      server:send("200 OK\n")
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "DELB" then
    local _, _, _, file, line = string.find(line, "^([A-Z]+)%s+(.-)%s+(%d+)%s*$")
    if file and line then
      remove_breakpoint(file, tonumber(line))
      server:send("200 OK\n")
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "EXEC" then
    -- extract any optional parameters
    local params = string.match(line, "--%s*(%b{})%s*$")
    local _, _, chunk = string.find(line, "^[A-Z]+%s+(.+)$")
    if chunk then
      local func, res = ildebug.loadstring(chunk)
      local status
      if func then
        local pfunc = params and loadstring("return "..params) -- use internal function
        params = pfunc and pfunc()
        params = (type(params) == "table" and params or {})
        local stack = tonumber(params.stack)
        -- if the requested stack frame is not the current one, then use a new capture
        -- with a specific stack frame: `capture_vars(0, coro_debugee)`

        -- TODO
        assert(not stack)
        stack = 0

        local env = capture_vars(stack_offset+1 + stack)
        setfenv(func, env)
        status, res = stringify_results(params, pcall(func, unpack(env['...'] or {})))
      end
      if status then
        server:send("200 OK " .. tostring(#res) .. "\n")
        server:send(res)
      else
        -- fix error if not set (for example, when loadstring is not present)
        if not res then res = "Unknown error" end
        server:send("401 Error in Expression " .. tostring(#res) .. "\n")
        server:send(res)
      end
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "LOAD" then
    local _, _, size, name = string.find(line, "^[A-Z]+%s+(%d+)%s+(%S.-)%s*$")
    size = tonumber(size)

    if abort == nil then -- no LOAD/RELOAD allowed inside start()
      if size > 0 then server:receive(size) end
      assert(info.file and info.line)
      server:send("201 Started " .. info.file .. " " .. tostring(info.line) .. "\n")
    else
      -- reset environment to allow required modules to load again
      -- remove those packages that weren't loaded when debugger started
      for k in pairs(package.loaded) do
        if not loaded[k] then package.loaded[k] = nil end
      end

      if size == 0 and name == '-' then -- RELOAD the current script being debugged
        server:send("200 OK 0\n")
        coroyield("load")
      else
        -- receiving 0 bytes blocks (at least in luasocket 2.0.2), so skip reading
        local chunk = size == 0 and "" or server:receive(size)
        if chunk then -- LOAD a new script for debugging
          local func, res = mobdebug.loadstring(chunk, "@"..name)
          if func then
            server:send("200 OK 0\n")
            debugee = func
            coroyield("load")
          else
            server:send("401 Error in Expression " .. tostring(#res) .. "\n")
            server:send(res)
          end
        else
          server:send("400 Bad Request\n")
        end
      end
    end
  elseif command == "SETW" then
    local _, _, exp = string.find(line, "^[A-Z]+%s+(.+)%s*$")
    if exp then
      local func, res = mobdebug.loadstring("return(" .. exp .. ")")
      if func then
        watchescnt = watchescnt + 1
        local newidx = #watches + 1
        watches[newidx] = func
        server:send("200 OK " .. tostring(newidx) .. "\n")
      else
        server:send("401 Error in Expression " .. tostring(#res) .. "\n")
        server:send(res)
      end
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "DELW" then
    local _, _, index = string.find(line, "^[A-Z]+%s+(%d+)%s*$")
    index = tonumber(index)
    if index > 0 and index <= #watches then
      watchescnt = watchescnt - (watches[index] ~= emptyWatch and 1 or 0)
      watches[index] = emptyWatch
      server:send("200 OK\n")
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "RUN" or command == "STEP" or command == "OVER" or command == "OUT" then
    server:send("200 OK\n")

    return cmd_results[command]
  elseif command == "BASEDIR" then
    local _, _, dir = string.find(line, "^[A-Z]+%s+(.+)%s*$")
    log("Base Directory: " .. dir)
    if dir then
      basedir = iscasepreserving and string.lower(dir) or dir
      -- reset cached source as it may change with basedir
      recalculate_file_info()
      server:send("200 OK\n")
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "SUSPEND" then
    return cmd_results.SUSPEND
  elseif command == "DONE" then
    debug.sethook() -- Disable debugging
    return cmd_results.RUN
  elseif command == "STACK" then
    -- first check if we can execute the stack command
    -- as it requires yielding back to debug_hook it cannot be executed
    -- if we have not seen the hook yet as happens after start().
    -- in this case we simply return an empty result
    local vars, ev = {}
    --[[if seen_hook then
      ev, vars = coroyield("stack")
    end
    if ev and ev ~= events.STACK then
      server:send("401 Error in Execution " .. tostring(#vars) .. "\n")
      server:send(vars)
    else--]]

    vars = stack(stack_offset + 1)

      local params = string.match(line, "--%s*(%b{})%s*$")
      local pfunc = params and loadstring("return "..params) -- use internal function
      params = pfunc and pfunc()
      params = (type(params) == "table" and params or {})
      if params.nocode == nil then params.nocode = true end
      if params.sparse == nil then params.sparse = false end
      -- take into account additional levels for the stack frames and data management
      if tonumber(params.maxlevel) then params.maxlevel = tonumber(params.maxlevel)+4 end

      local ok, res = pcall(serpent.dump, vars, params)
      if ok then
        server:send("200 OK " .. tostring(res) .. "\n")
      else
        server:send("401 Error in Execution " .. tostring(#res) .. "\n")
        server:send(res)
      end
    --[[ end --]]

    -- TODO restore_vars

  elseif command == "OUTPUT" then
    local _, _, stream, mode = string.find(line, "^[A-Z]+%s+(%w+)%s+([dcr])%s*$")
    if stream and mode and stream == "stdout" then
      -- assign "print" in the global environment
      local default = mode == 'd'
      genv.print = default and iobase.print or corowrap(function()
        -- wrapping into coroutine.wrap protects this function from
        -- being stepped through in the debugger.
        -- don't use vararg (...) as it adds a reference for its values,
        -- which may affect how they are garbage collected
        while true do
          local tbl = {coroutine.yield()}
          if mode == 'c' then iobase.print(unpack(tbl)) end
          for n = 1, #tbl do
            tbl[n] = select(2, pcall(mobdebug.line, tbl[n], {nocode = true, comment = false})) end
          local file = table.concat(tbl, "\t").."\n"
          server:send("204 Output " .. stream .. " " .. tostring(#file) .. "\n" .. file)
        end
      end)
      if not default then genv.print() end -- "fake" print to start printing loop
      server:send("200 OK\n")
    else
      server:send("400 Bad Request\n")
    end
  elseif command == "EXIT" then
    server:send("200 OK\n")
    debug.sethook() -- Disable debugging
    ildebug.exit()
    return cmd_results.RUN
  else
    server:send("400 Bad Request\n")
  end

  -- TODO use enum constant
  return "read"
end

return process_cmd
