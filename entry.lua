local dbg_env = {
  DbgModPath = ModPath,
  pcall = blt.pcall,
}
setmetatable(dbg_env, {
  __index = _G
})
dbg_env._DBG = dbg_env

-- Set if you're compiling your own copy of pd2-luasocket inside the mod directory
local using_custom_native = false

function dbg_env.loadfile(path)
  path = DbgModPath .. path
  local file = io.open(path, "r")
  local buff = file:read("*all")
  file:close()

  local f, err = assert(loadstring(buff, path))

  if f then
    setfenv(f, dbg_env)
  end

  return f, err
end
setfenv(dbg_env.loadfile, dbg_env)

local plat = blt.blt_info().platform
local sock_fn

local native_dir = using_custom_native and "pd2-luasocket/build/" .. (plat == "mswindows" and "Debug/") or ""

if plat == "gnu+linux" then
  sock_fn = native_dir .. "libpd2-luasocket.so"
elseif plat == "mswindows" then
  sock_fn = native_dir .. "pd2-luasocket.dll"
else
  error("Unknown platform " .. tostring(plat))
end

local status, socket, extras = blt.load_native(ModPath .. sock_fn)

local mobdebug = dbg_env.loadfile("ildebug.lua")()

local function exit()
  extras.exit(0)
end

mobdebug.loadstring = loadstring
mobdebug.socket = socket
mobdebug.exit = exit
mobdebug.connecttimeout = 2
mobdebug:start()

-- Stuff to test the debugger on
-- FIXME Currently it pauses immeidately after starting the debugger
-- Just hit resume (F5) and keep going if you're not working on the debugger
local function f(a)
  --log(a)
end

BLT.Mods:GetMod("hi")

f("Hello, World!")
f(f(hi) or "z")
