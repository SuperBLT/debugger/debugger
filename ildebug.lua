local ildebug = {
  port = 8172,
}
_DBG.ildebug = ildebug

-- Options for debugging the debugger
-- You can't actually debug the debugger with the debugger, but this
--  allows for easier printf-style debugging
local debug_debugger_opt = {
  -- Print all the communications sent between the IDE and the debugger
  log_socket_comms = false,

  -- Wrap the debug hook in a pcall
  pcall_wrap = true, --false,
}

local _enum_mt = {
  __index = function(tbl, key) error("Unknown key \"" .. tostring(key) .. "\"", 2) end,
}

local states = {
  -- Waiting for the server to send us a command, pause on the next line without sending anything
  INITIAL = "initial",

  -- Running, don't stop on the debug events
  RUNNING = "running",

  -- Pause at the next line
  PAUSED = "paused",

  -- Pause at the specified level, or below
  PAUSE_AT_LEVEL = "pause_at_level",
}

_DBG.cmd_results = {
  -- The appliation was paused while it was running
  SUSPEND = "suspend",

  RUN = "run",
  STEP = "step_into",
  OVER = "step_over",
  OUT = "step_out",
}

setmetatable(states, _enum_mt)
setmetatable(cmd_results, _enum_mt)

_DBG.server = nil
_DBG.breakpoints = {}
_DBG.basedir = ""
_DBG.file_info = nil
local stack_level = nil

-- Stack index to get the top of the application stack
-- This needs to be changed when using a pcall() wrapper around debug_hook
_DBG.stack_offset = 2

-- Always use the same case
local iscasepreserving = false

----

local process_cmd = loadfile("commands.lua")(ildebug)

local function has_breakpoint(file, line)
  return breakpoints[line] and breakpoints[line][file]
end

local function restore_vars(vars)
  if type(vars) ~= 'table' then return end

  -- locals need to be processed in the reverse order, starting from
  -- the inner block out, to make sure that the localized variables
  -- are correctly updated with only the closest variable with
  -- the same name being changed
  -- first loop find how many local variables there is, while
  -- the second loop processes them from i to 1
  local i = 1
  while true do
    local name = debug.getlocal(3, i)
    if not name then break end
    i = i + 1
  end
  i = i - 1
  local written_vars = {}
  while i > 0 do
    local name = debug.getlocal(3, i)
    if not written_vars[name] then
      if string.sub(name, 1, 1) ~= '(' then
        debug.setlocal(3, i, rawget(vars, name))
      end
      written_vars[name] = true
    end
    i = i - 1
  end

  i = 1
  local func = debug.getinfo(3, "f").func
  while true do
    local name = debug.getupvalue(func, i)
    if not name then break end
    if not written_vars[name] then
      if string.sub(name, 1, 1) ~= '(' then
        debug.setupvalue(func, i, rawget(vars, name))
      end
      written_vars[name] = true
    end
    i = i + 1
  end
end

function _DBG.capture_vars(level, thread)
  level = level+1 -- add two levels for this and debug calls
  local info = thread and debug.getinfo(thread, level, "f") or debug.getinfo(level, "f")
  local func = (info or {}).func
  if not func then return {} end

  local vars = {['...'] = {}}
  local i = 1
  while true do
    local name, value = debug.getupvalue(func, i)
    if not name then break end
    if string.sub(name, 1, 1) ~= '(' then vars[name] = value end
    i = i + 1
  end
  i = 1
  while true do
    local name, value
    if thread then
      name, value = debug.getlocal(thread, level, i)
    else
      name, value = debug.getlocal(level, i)
    end
    if not name then break end
    if string.sub(name, 1, 1) ~= '(' then vars[name] = value end
    i = i + 1
  end
  -- get varargs (these use negative indices)
  i = 1
  while true do
    local name, value
    if thread then
      name, value = debug.getlocal(thread, level, -i)
    else
      name, value = debug.getlocal(level, -i)
    end
    -- `not name` should be enough, but LuaJIT 2.0.0 incorrectly reports `(*temporary)` names here
    if not name or name ~= "(*vararg)" then break end
    vars['...'][i] = value
    i = i + 1
  end
  -- returned 'vars' table plays a dual role: (1) it captures local values
  -- and upvalues to be restored later (in case they are modified in "eval"),
  -- and (2) it provides an environment for evaluated chunks.
  -- getfenv(func) is needed to provide proper environment for functions,
  -- including access to globals, but this causes vars[name] to fail in
  -- restore_vars on local variables or upvalues with `nil` values when
  -- 'strict' is in effect. To avoid this `rawget` is used in restore_vars.
  setmetatable(vars, { __index = getfenv(func), __newindex = getfenv(func), __mode = "v" })
  return vars
end

local function stack_depth(start_depth)
  if start_depth then
    for i = start_depth, 0, -1 do
      if debug.getinfo(i, "l") then return i+1 end
    end
    return start_depth
  else
    local i = 1
    while true do
      if not debug.getinfo(i, "l") then return i end
      i = i + 1
    end
  end
end

function _DBG.removebasedir(path)
  if path:sub(1,1) == "@" then
    path = path:sub(2)
  end

  if path:sub(1, basedir:len()) == basedir then
    return path:sub(basedir:len() + 1)
  else
    return path
  end
end

----
local state = states.INITIAL
local state_data = {}

local function should_pause(event)
  return event ~= "line" and server:is_available()
end

function _DBG.recalculate_file_info()
  local caller = file_info.caller

  file_info.file = removebasedir(caller.source)
end

local function reset_file_info(caller)
  local file = caller.source

  file_info = {
    caller = caller
  }

  recalculate_file_info()
end

local function debug_hook(event, line)
  if event ~= "line" then
    file_info = nil
  end

  if not file_info then
    reset_file_info(debug.getinfo(stack_offset, "S"))
  end

  local file = file_info.file

  -- TODO handle this for PAUSE_AT_LEVEL
  -- Here, we stop processing if we're currently running. This should
  --  cause fairly minimal performance loss, since almost every hook
  --  call is a line call, and it ends up as only two ifs
  if state == states.RUNNING or state == states.PAUSE_AT_LEVEL then
    if should_pause(event) or has_breakpoint(file, line) then
      state = states.PAUSED

      -- We haven't been keeping track of the stack level or fil
      --  information, so disregard the old stuff.
      stack_level = nil
      file_info = nil
    end

    if state == states.RUNNING then
      return
    end
  end

  -- Keep track of (roughly) how many stack levels deep we are - see the caviets
  --  to this approach below. If only Lua just let us ask it how deep the stack
  --  is, that would save an awful lot of trouble.
  --
  -- At this point we are sure we're not running, so performance here isn't as
  --  critical. However, the user may want to run large amounts of code with
  --  run-to-cursor, so this is still very important performance wise.
  -- TODO should these be handled in some way, for function breakpoints?
  if event == "call" then
    stack_level = stack_level and stack_level + 1
    file_info = nil
    return
  elseif event == "return" or event == "tail return" then
    -- FIXME do tail returns really decrease the stack?
    stack_level = stack_level and stack_level - 1
    file_info = nil
    return
  end

  -- this is needed to check if the stack got shorter or longer.
  -- unfortunately counting call/return calls is not reliable.
  -- the discrepancy may happen when "pcall(load, '')" call is made
  -- or when "error()" is called in a function.
  -- in either case there are more "call" than "return" events reported.
  -- this validation is done for every "line" event, but should be "cheap"
  -- as it checks for the stack to get shorter (or longer by one call).
  -- start from one level higher just in case we need to grow the stack.
  -- this may happen after coroutine.resume call to a function that doesn't
  -- have any other instructions to execute. it triggers three returns:
  -- "return, tail return, return", which needs to be accounted for.
  --
  -- If something causes the stack level to become unknown (eg running and
  --  not taking note of it for performance reasons), that must set stack_level
  --  to nil, and it'll then be rediscovered from scratch.
  stack_level = stack_depth(stack_level and stack_level+1)

  local send_state = true
  if state == states.INITIAL then
    state = states.PAUSED
    send_state = false
  elseif state == states.PAUSED then
  elseif state == states.PAUSE_AT_LEVEL then
    if stack_level > state_data.level then
      return
    end
  else
    error("Unknown state " .. tostring(state))
  end

  if send_state then
    --local ev, _, _, _, idx_watch = coroyield()
    --if ev == events.BREAK then
      server:send("202 Paused " .. file .. " " .. tostring(line) .. "\n")
    --elseif ev == events.WATCH then
    --  server:send("203 Paused " .. file .. " " .. tostring(line) .. " " .. tostring(idx_watch) .. "\n")
    --elseif ev == events.RESTART then
    --  -- nothing to do
    --else
    --  server:send("401 Error in Execution " .. tostring(#file) .. "\n")
    --  server:send(file)
    --end
  end

  -- Not currently used
  --local vars = capture_vars(stack_offset)

  while true do
    local res = process_cmd({
        file = file,
        line = line,
    })

    if res == cmd_results.SUSPEND then
      state = states.PAUSED
    elseif res == "read" then
      -- Keep the loop going, to process another command
    elseif res == cmd_results.RUN then
      state = states.RUNNING
      break
    elseif res == cmd_results.STEP then
      -- Make sure we pause at the next line, and continue
      state = states.PAUSED
      break
    elseif res == cmd_results.OVER then
      state = states.PAUSE_AT_LEVEL
      state_data = { level = stack_level }
      break
    elseif res == cmd_results.OUT then
      state = states.PAUSE_AT_LEVEL
      state_data = { level = stack_level - 1 }
      break
    else
      error("Unknown status " .. tostring(res))
    end
  end
end

local function connect(controller_host, controller_port)
  local sock, err = ildebug.socket.tcp()
  if not sock then return nil, err end

  if sock.settimeout then sock:settimeout(ildebug.connecttimeout) end
  local res, err = sock:connect(controller_host, tostring(controller_port))
  if sock.settimeout then sock:settimeout() end

  if not res then return nil, err end
  return sock
end

function ildebug:start(controller_host, controller_port)
  controller_host = lasthost or "localhost"
  controller_port = lastport or ildebug.port

  local real_server = assert(connect(controller_host, controller_port))

  server = {}
  function server:settimeout(timeout)
    real_server:settimeout(timeout)
  end
  function server:is_available()
    if self._buf then
      return true
    end

    real_server:settimeout(0) -- non-blocking
    self._buf = real_server:receive(1)
    real_server:settimeout() -- back to blocking

    return self._buf and true or false
  end
  function server:receive(mode)
    if self._buf then
      real_server:settimeout()
    end

    local line, err = real_server:receive(mode)

    if line and self._buf then
      line = self._buf .. line
      self._buf = nil
    end

    if debug_debugger_opt.log_socket_comms and err ~= "timeout" then
      log("IDE: " .. (line or err))
    end

    return line, err
  end
  function server:send(data)
    if debug_debugger_opt.log_socket_comms then
      local trimmed = tostring(data):gsub("(.-)[\n]*$", "%1")
      log("ildebug: " .. trimmed)
    end
    return real_server:send(data)
  end

  if debug_debugger_opt.pcall_wrap then
    -- pcall wrapper
    -- perf hit, good for debugging the debugger
    stack_offset = stack_offset + 2 -- Make up for the wrapper
    debug.sethook(function(...)
        local success, msg = _G.pcall(debug_hook, ...)
        if not success and msg then
          log("Error: " .. tostring(msg))
        end
    end, "lcr")
  else
    debug.sethook(debug_hook, "lcr")
  end
end

return ildebug
